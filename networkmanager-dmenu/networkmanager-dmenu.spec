Name:           networkmanager-dmenu
Version:        2.5.0
Release:        1%{?dist}
Summary:        Manage NetworkManager connections with dmenu instead of nm-applet

License:        MIT
URL:            https://github.com/firecat53/networkmanager-dmenu
Source0:        https://github.com/firecat53/networkmanager-dmenu/archive/v%{version}/networkmanager-dmenu-%{version}.tar.gz

BuildArch:      noarch

Requires:       libnma
Requires:       python3-gobject

Recommends:     pinentry
Recommends:     notify-send

%description
Manage NetworkManager connections with dmenu instead of nm-applet
Features:
- Connect to existing NetworkManager wifi or wired connections
- Connect to new wifi connections. Requests passphrase if required
- Connect to _existing_ VPN, Wireguard, GSM/WWAN and Bluetooth connections
- Enable/Disable wifi, WWAN, bluetooth and networking
- Launch nm-connection-editor GUI
- Support for multiple wifi adapters
- Optional Pinentry support for secure passphrase entry
- Delete existing connections
- Rescan wifi networks
- Uses notify-send for notifications if available


%prep
%autosetup

%build

%install
install -D -pm 755 networkmanager_dmenu %{buildroot}%{_bindir}/networkmanager-dmenu
install -D -pm 644 networkmanager_dmenu.desktop %{buildroot}%{_datadir}/applications/networkmanager-dmenu.desktop

ln -s networkmanager-dmenu %{buildroot}%{_bindir}/networkmanager_dmenu

%check


%files
%defattr(-,root,root,-)
%license LICENSE.txt
%doc README.md
%doc config.ini.example
%{_bindir}/*
%{_datadir}/applications/*

%changelog
* Mon Nov 11 2024 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.5.0-1
- Update to 2.5.0

* Thu Sep 08 2022 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.1.0-1
- Update to 2.1.0
* Thu Dec 16 2021 Jerzy Drożdż <jerzy.drozdz@jdsieci.pl> - 2.0.1-1
- Update to 2.0.1
* Thu Dec 02 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.6.1-3
- Example config moved to docs
* Thu Dec 02 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.6.1-2
- Fixed default attributes
* Thu Dec 02 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.6.1-1
- Initial build
